#Maven репозиторий для хранения java библиотек КриптоПро версии java-csp-5.0.40621-A

#блок <dependency/> 
#вставить в pom.xml в блок <dependencies/>
<dependency>
  <groupId>ru.cryptopro.unofficial</groupId>
  <artifactId>crypto-pro-repo</artifactId>
  <version>1.0-SNAPSHOT</version>
</dependency>

#загрузка зависимости в локальный репозиторий
mvn dependency:get -Dartifact=ru.cryptopro.unofficial:crypto-pro-repo:1.0-SNAPSHOT

#настройка удаленного репозитория для скачивания записисмости
#вставить в pom.xml в блок </project>
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/23966508/packages/maven</url>
  </repository>
</repositories>

<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/23966508/packages/maven</url>
  </repository>

  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/23966508/packages/maven</url>
  </snapshotRepository>
</distributionManagement>